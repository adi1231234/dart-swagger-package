//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'feed_option.g.dart';



abstract class FeedOption implements Built<FeedOption, FeedOptionBuilder> {
    @BuiltValueField(wireName: r'rank')
    num get rank;

    @BuiltValueField(wireName: r'type')
    FeedOptionTypeEnum get type;
    // enum typeEnum {  Category,  RecommendedCategories,  RecommendedProducts,  Sale,  };

    @BuiltValueField(wireName: r'userId')
    num get userId;

    @BuiltValueField(wireName: r'config')
    num get config;

    FeedOption._();

    static void _initializeBuilder(FeedOptionBuilder b) => b;

    factory FeedOption([void updates(FeedOptionBuilder b)]) = _$FeedOption;

    @BuiltValueSerializer(custom: true)
    static Serializer<FeedOption> get serializer => _$FeedOptionSerializer();
}

class _$FeedOptionSerializer implements StructuredSerializer<FeedOption> {
    @override
    final Iterable<Type> types = const [FeedOption, _$FeedOption];

    @override
    final String wireName = r'FeedOption';

    @override
    Iterable<Object?> serialize(Serializers serializers, FeedOption object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'rank')
            ..add(serializers.serialize(object.rank,
                specifiedType: const FullType(num)));
        result
            ..add(r'type')
            ..add(serializers.serialize(object.type,
                specifiedType: const FullType(FeedOptionTypeEnum)));
        result
            ..add(r'userId')
            ..add(serializers.serialize(object.userId,
                specifiedType: const FullType(num)));
        result
            ..add(r'config')
            ..add(serializers.serialize(object.config,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    FeedOption deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = FeedOptionBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'rank':
                    result.rank = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'type':
                    result.type = serializers.deserialize(value,
                        specifiedType: const FullType(FeedOptionTypeEnum)) as FeedOptionTypeEnum;
                    break;
                case r'userId':
                    result.userId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'config':
                    result.config = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

class FeedOptionTypeEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'Category')
  static const FeedOptionTypeEnum category = _$feedOptionTypeEnum_category;
  @BuiltValueEnumConst(wireName: r'RecommendedCategories')
  static const FeedOptionTypeEnum recommendedCategories = _$feedOptionTypeEnum_recommendedCategories;
  @BuiltValueEnumConst(wireName: r'RecommendedProducts')
  static const FeedOptionTypeEnum recommendedProducts = _$feedOptionTypeEnum_recommendedProducts;
  @BuiltValueEnumConst(wireName: r'Sale')
  static const FeedOptionTypeEnum sale = _$feedOptionTypeEnum_sale;

  static Serializer<FeedOptionTypeEnum> get serializer => _$feedOptionTypeEnumSerializer;

  const FeedOptionTypeEnum._(String name): super(name);

  static BuiltSet<FeedOptionTypeEnum> get values => _$feedOptionTypeEnumValues;
  static FeedOptionTypeEnum valueOf(String name) => _$feedOptionTypeEnumValueOf(name);
}

