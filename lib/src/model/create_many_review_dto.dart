//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/review.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_many_review_dto.g.dart';



abstract class CreateManyReviewDto implements Built<CreateManyReviewDto, CreateManyReviewDtoBuilder> {
    @BuiltValueField(wireName: r'bulk')
    BuiltList<Review> get bulk;

    CreateManyReviewDto._();

    static void _initializeBuilder(CreateManyReviewDtoBuilder b) => b;

    factory CreateManyReviewDto([void updates(CreateManyReviewDtoBuilder b)]) = _$CreateManyReviewDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateManyReviewDto> get serializer => _$CreateManyReviewDtoSerializer();
}

class _$CreateManyReviewDtoSerializer implements StructuredSerializer<CreateManyReviewDto> {
    @override
    final Iterable<Type> types = const [CreateManyReviewDto, _$CreateManyReviewDto];

    @override
    final String wireName = r'CreateManyReviewDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateManyReviewDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'bulk')
            ..add(serializers.serialize(object.bulk,
                specifiedType: const FullType(BuiltList, [FullType(Review)])));
        return result;
    }

    @override
    CreateManyReviewDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateManyReviewDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'bulk':
                    result.bulk.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Review)])) as BuiltList<Review>);
                    break;
            }
        }
        return result.build();
    }
}

