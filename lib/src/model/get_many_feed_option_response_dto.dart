//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/feed_option.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_many_feed_option_response_dto.g.dart';



abstract class GetManyFeedOptionResponseDto implements Built<GetManyFeedOptionResponseDto, GetManyFeedOptionResponseDtoBuilder> {
    @BuiltValueField(wireName: r'data')
    BuiltList<FeedOption> get data;

    @BuiltValueField(wireName: r'count')
    num get count;

    @BuiltValueField(wireName: r'total')
    num get total;

    @BuiltValueField(wireName: r'page')
    num get page;

    @BuiltValueField(wireName: r'pageCount')
    num get pageCount;

    GetManyFeedOptionResponseDto._();

    static void _initializeBuilder(GetManyFeedOptionResponseDtoBuilder b) => b;

    factory GetManyFeedOptionResponseDto([void updates(GetManyFeedOptionResponseDtoBuilder b)]) = _$GetManyFeedOptionResponseDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<GetManyFeedOptionResponseDto> get serializer => _$GetManyFeedOptionResponseDtoSerializer();
}

class _$GetManyFeedOptionResponseDtoSerializer implements StructuredSerializer<GetManyFeedOptionResponseDto> {
    @override
    final Iterable<Type> types = const [GetManyFeedOptionResponseDto, _$GetManyFeedOptionResponseDto];

    @override
    final String wireName = r'GetManyFeedOptionResponseDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, GetManyFeedOptionResponseDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'data')
            ..add(serializers.serialize(object.data,
                specifiedType: const FullType(BuiltList, [FullType(FeedOption)])));
        result
            ..add(r'count')
            ..add(serializers.serialize(object.count,
                specifiedType: const FullType(num)));
        result
            ..add(r'total')
            ..add(serializers.serialize(object.total,
                specifiedType: const FullType(num)));
        result
            ..add(r'page')
            ..add(serializers.serialize(object.page,
                specifiedType: const FullType(num)));
        result
            ..add(r'pageCount')
            ..add(serializers.serialize(object.pageCount,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    GetManyFeedOptionResponseDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = GetManyFeedOptionResponseDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'data':
                    result.data.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(FeedOption)])) as BuiltList<FeedOption>);
                    break;
                case r'count':
                    result.count = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'total':
                    result.total = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'page':
                    result.page = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'pageCount':
                    result.pageCount = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

