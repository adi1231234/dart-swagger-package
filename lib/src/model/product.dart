//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:products_communicator/src/model/availability_time.dart';
import 'package:products_communicator/src/model/product_type.dart';
import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/price_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'product.g.dart';



abstract class Product implements Built<Product, ProductBuilder> {
    @BuiltValueField(wireName: r'id')
    num get id;

    @BuiltValueField(wireName: r'name')
    String get name;

    @BuiltValueField(wireName: r'description')
    String get description;

    @BuiltValueField(wireName: r'imagePaths')
    BuiltList<String> get imagePaths;

    @BuiltValueField(wireName: r'price')
    num get price;

    @BuiltValueField(wireName: r'priceType')
    PriceType get priceType;
    // enum priceTypeEnum {  daily,  hourly,  };

    @BuiltValueField(wireName: r'ownerId')
    String get ownerId;

    @BuiltValueField(wireName: r'productType')
    ProductType get productType;
    // enum productTypeEnum {  onSell,  request,  };

    @BuiltValueField(wireName: r'categoryId')
    num get categoryId;

    @BuiltValueField(wireName: r'availabilityTimes')
    BuiltList<AvailabilityTime> get availabilityTimes;

    @BuiltValueField(wireName: r'suspended')
    bool get suspended;

    Product._();

    static void _initializeBuilder(ProductBuilder b) => b
        ..suspended = false;

    factory Product([void updates(ProductBuilder b)]) = _$Product;

    @BuiltValueSerializer(custom: true)
    static Serializer<Product> get serializer => _$ProductSerializer();
}

class _$ProductSerializer implements StructuredSerializer<Product> {
    @override
    final Iterable<Type> types = const [Product, _$Product];

    @override
    final String wireName = r'Product';

    @override
    Iterable<Object?> serialize(Serializers serializers, Product object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'id')
            ..add(serializers.serialize(object.id,
                specifiedType: const FullType(num)));
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        result
            ..add(r'description')
            ..add(serializers.serialize(object.description,
                specifiedType: const FullType(String)));
        result
            ..add(r'imagePaths')
            ..add(serializers.serialize(object.imagePaths,
                specifiedType: const FullType(BuiltList, [FullType(String)])));
        result
            ..add(r'price')
            ..add(serializers.serialize(object.price,
                specifiedType: const FullType(num)));
        result
            ..add(r'priceType')
            ..add(serializers.serialize(object.priceType,
                specifiedType: const FullType(PriceType)));
        result
            ..add(r'ownerId')
            ..add(serializers.serialize(object.ownerId,
                specifiedType: const FullType(String)));
        result
            ..add(r'productType')
            ..add(serializers.serialize(object.productType,
                specifiedType: const FullType(ProductType)));
        result
            ..add(r'categoryId')
            ..add(serializers.serialize(object.categoryId,
                specifiedType: const FullType(num)));
        result
            ..add(r'availabilityTimes')
            ..add(serializers.serialize(object.availabilityTimes,
                specifiedType: const FullType(BuiltList, [FullType(AvailabilityTime)])));
        result
            ..add(r'suspended')
            ..add(serializers.serialize(object.suspended,
                specifiedType: const FullType(bool)));
        return result;
    }

    @override
    Product deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ProductBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'description':
                    result.description = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'imagePaths':
                    result.imagePaths.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(String)])) as BuiltList<String>);
                    break;
                case r'price':
                    result.price = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'priceType':
                    result.priceType = serializers.deserialize(value,
                        specifiedType: const FullType(PriceType)) as PriceType;
                    break;
                case r'ownerId':
                    result.ownerId = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'productType':
                    result.productType = serializers.deserialize(value,
                        specifiedType: const FullType(ProductType)) as ProductType;
                    break;
                case r'categoryId':
                    result.categoryId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'availabilityTimes':
                    result.availabilityTimes.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(AvailabilityTime)])) as BuiltList<AvailabilityTime>);
                    break;
                case r'suspended':
                    result.suspended = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
            }
        }
        return result.build();
    }
}

