//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/category.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_many_category_dto.g.dart';



abstract class CreateManyCategoryDto implements Built<CreateManyCategoryDto, CreateManyCategoryDtoBuilder> {
    @BuiltValueField(wireName: r'bulk')
    BuiltList<Category> get bulk;

    CreateManyCategoryDto._();

    static void _initializeBuilder(CreateManyCategoryDtoBuilder b) => b;

    factory CreateManyCategoryDto([void updates(CreateManyCategoryDtoBuilder b)]) = _$CreateManyCategoryDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateManyCategoryDto> get serializer => _$CreateManyCategoryDtoSerializer();
}

class _$CreateManyCategoryDtoSerializer implements StructuredSerializer<CreateManyCategoryDto> {
    @override
    final Iterable<Type> types = const [CreateManyCategoryDto, _$CreateManyCategoryDto];

    @override
    final String wireName = r'CreateManyCategoryDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateManyCategoryDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'bulk')
            ..add(serializers.serialize(object.bulk,
                specifiedType: const FullType(BuiltList, [FullType(Category)])));
        return result;
    }

    @override
    CreateManyCategoryDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateManyCategoryDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'bulk':
                    result.bulk.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Category)])) as BuiltList<Category>);
                    break;
            }
        }
        return result.build();
    }
}

