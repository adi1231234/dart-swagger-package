//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/json_object.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'query_dto.g.dart';



abstract class QueryDTO implements Built<QueryDTO, QueryDTOBuilder> {
    @BuiltValueField(wireName: r'query')
    JsonObject get query;

    QueryDTO._();

    static void _initializeBuilder(QueryDTOBuilder b) => b;

    factory QueryDTO([void updates(QueryDTOBuilder b)]) = _$QueryDTO;

    @BuiltValueSerializer(custom: true)
    static Serializer<QueryDTO> get serializer => _$QueryDTOSerializer();
}

class _$QueryDTOSerializer implements StructuredSerializer<QueryDTO> {
    @override
    final Iterable<Type> types = const [QueryDTO, _$QueryDTO];

    @override
    final String wireName = r'QueryDTO';

    @override
    Iterable<Object?> serialize(Serializers serializers, QueryDTO object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'query')
            ..add(serializers.serialize(object.query,
                specifiedType: const FullType(JsonObject)));
        return result;
    }

    @override
    QueryDTO deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = QueryDTOBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'query':
                    result.query = serializers.deserialize(value,
                        specifiedType: const FullType(JsonObject)) as JsonObject;
                    break;
            }
        }
        return result.build();
    }
}

