//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'suspend_product_dto.g.dart';



abstract class SuspendProductDto implements Built<SuspendProductDto, SuspendProductDtoBuilder> {
    @BuiltValueField(wireName: r'productId')
    num get productId;

    SuspendProductDto._();

    static void _initializeBuilder(SuspendProductDtoBuilder b) => b;

    factory SuspendProductDto([void updates(SuspendProductDtoBuilder b)]) = _$SuspendProductDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<SuspendProductDto> get serializer => _$SuspendProductDtoSerializer();
}

class _$SuspendProductDtoSerializer implements StructuredSerializer<SuspendProductDto> {
    @override
    final Iterable<Type> types = const [SuspendProductDto, _$SuspendProductDto];

    @override
    final String wireName = r'SuspendProductDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, SuspendProductDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'productId')
            ..add(serializers.serialize(object.productId,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    SuspendProductDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = SuspendProductDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'productId':
                    result.productId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

