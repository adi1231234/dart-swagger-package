//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'saved_product.g.dart';



abstract class SavedProduct implements Built<SavedProduct, SavedProductBuilder> {
    @BuiltValueField(wireName: r'productId')
    num get productId;

    @BuiltValueField(wireName: r'userId')
    num get userId;

    SavedProduct._();

    static void _initializeBuilder(SavedProductBuilder b) => b;

    factory SavedProduct([void updates(SavedProductBuilder b)]) = _$SavedProduct;

    @BuiltValueSerializer(custom: true)
    static Serializer<SavedProduct> get serializer => _$SavedProductSerializer();
}

class _$SavedProductSerializer implements StructuredSerializer<SavedProduct> {
    @override
    final Iterable<Type> types = const [SavedProduct, _$SavedProduct];

    @override
    final String wireName = r'SavedProduct';

    @override
    Iterable<Object?> serialize(Serializers serializers, SavedProduct object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'productId')
            ..add(serializers.serialize(object.productId,
                specifiedType: const FullType(num)));
        result
            ..add(r'userId')
            ..add(serializers.serialize(object.userId,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    SavedProduct deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = SavedProductBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'productId':
                    result.productId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'userId':
                    result.userId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

