//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:products_communicator/src/model/review_target_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'review.g.dart';



abstract class Review implements Built<Review, ReviewBuilder> {
    @BuiltValueField(wireName: r'freeText')
    String get freeText;

    @BuiltValueField(wireName: r'stars')
    num get stars;

    @BuiltValueField(wireName: r'authorId')
    num get authorId;

    @BuiltValueField(wireName: r'targetId')
    num get targetId;

    @BuiltValueField(wireName: r'targetType')
    ReviewTargetType get targetType;
    // enum targetTypeEnum {  product,  user,  };

    Review._();

    static void _initializeBuilder(ReviewBuilder b) => b;

    factory Review([void updates(ReviewBuilder b)]) = _$Review;

    @BuiltValueSerializer(custom: true)
    static Serializer<Review> get serializer => _$ReviewSerializer();
}

class _$ReviewSerializer implements StructuredSerializer<Review> {
    @override
    final Iterable<Type> types = const [Review, _$Review];

    @override
    final String wireName = r'Review';

    @override
    Iterable<Object?> serialize(Serializers serializers, Review object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'freeText')
            ..add(serializers.serialize(object.freeText,
                specifiedType: const FullType(String)));
        result
            ..add(r'stars')
            ..add(serializers.serialize(object.stars,
                specifiedType: const FullType(num)));
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(num)));
        result
            ..add(r'targetId')
            ..add(serializers.serialize(object.targetId,
                specifiedType: const FullType(num)));
        result
            ..add(r'targetType')
            ..add(serializers.serialize(object.targetType,
                specifiedType: const FullType(ReviewTargetType)));
        return result;
    }

    @override
    Review deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ReviewBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'freeText':
                    result.freeText = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'stars':
                    result.stars = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'authorId':
                    result.authorId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'targetId':
                    result.targetId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'targetType':
                    result.targetType = serializers.deserialize(value,
                        specifiedType: const FullType(ReviewTargetType)) as ReviewTargetType;
                    break;
            }
        }
        return result.build();
    }
}

