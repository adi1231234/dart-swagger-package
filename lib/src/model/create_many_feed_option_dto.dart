//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/feed_option.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_many_feed_option_dto.g.dart';



abstract class CreateManyFeedOptionDto implements Built<CreateManyFeedOptionDto, CreateManyFeedOptionDtoBuilder> {
    @BuiltValueField(wireName: r'bulk')
    BuiltList<FeedOption> get bulk;

    CreateManyFeedOptionDto._();

    static void _initializeBuilder(CreateManyFeedOptionDtoBuilder b) => b;

    factory CreateManyFeedOptionDto([void updates(CreateManyFeedOptionDtoBuilder b)]) = _$CreateManyFeedOptionDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateManyFeedOptionDto> get serializer => _$CreateManyFeedOptionDtoSerializer();
}

class _$CreateManyFeedOptionDtoSerializer implements StructuredSerializer<CreateManyFeedOptionDto> {
    @override
    final Iterable<Type> types = const [CreateManyFeedOptionDto, _$CreateManyFeedOptionDto];

    @override
    final String wireName = r'CreateManyFeedOptionDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateManyFeedOptionDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'bulk')
            ..add(serializers.serialize(object.bulk,
                specifiedType: const FullType(BuiltList, [FullType(FeedOption)])));
        return result;
    }

    @override
    CreateManyFeedOptionDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateManyFeedOptionDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'bulk':
                    result.bulk.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(FeedOption)])) as BuiltList<FeedOption>);
                    break;
            }
        }
        return result.build();
    }
}

