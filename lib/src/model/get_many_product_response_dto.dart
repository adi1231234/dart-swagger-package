//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:products_communicator/src/model/product.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_many_product_response_dto.g.dart';



abstract class GetManyProductResponseDto implements Built<GetManyProductResponseDto, GetManyProductResponseDtoBuilder> {
    @BuiltValueField(wireName: r'data')
    BuiltList<Product> get data;

    @BuiltValueField(wireName: r'count')
    num get count;

    @BuiltValueField(wireName: r'total')
    num get total;

    @BuiltValueField(wireName: r'page')
    num get page;

    @BuiltValueField(wireName: r'pageCount')
    num get pageCount;

    GetManyProductResponseDto._();

    static void _initializeBuilder(GetManyProductResponseDtoBuilder b) => b;

    factory GetManyProductResponseDto([void updates(GetManyProductResponseDtoBuilder b)]) = _$GetManyProductResponseDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<GetManyProductResponseDto> get serializer => _$GetManyProductResponseDtoSerializer();
}

class _$GetManyProductResponseDtoSerializer implements StructuredSerializer<GetManyProductResponseDto> {
    @override
    final Iterable<Type> types = const [GetManyProductResponseDto, _$GetManyProductResponseDto];

    @override
    final String wireName = r'GetManyProductResponseDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, GetManyProductResponseDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'data')
            ..add(serializers.serialize(object.data,
                specifiedType: const FullType(BuiltList, [FullType(Product)])));
        result
            ..add(r'count')
            ..add(serializers.serialize(object.count,
                specifiedType: const FullType(num)));
        result
            ..add(r'total')
            ..add(serializers.serialize(object.total,
                specifiedType: const FullType(num)));
        result
            ..add(r'page')
            ..add(serializers.serialize(object.page,
                specifiedType: const FullType(num)));
        result
            ..add(r'pageCount')
            ..add(serializers.serialize(object.pageCount,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    GetManyProductResponseDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = GetManyProductResponseDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'data':
                    result.data.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Product)])) as BuiltList<Product>);
                    break;
                case r'count':
                    result.count = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'total':
                    result.total = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'page':
                    result.page = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'pageCount':
                    result.pageCount = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

