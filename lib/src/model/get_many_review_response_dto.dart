//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/review.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_many_review_response_dto.g.dart';



abstract class GetManyReviewResponseDto implements Built<GetManyReviewResponseDto, GetManyReviewResponseDtoBuilder> {
    @BuiltValueField(wireName: r'data')
    BuiltList<Review> get data;

    @BuiltValueField(wireName: r'count')
    num get count;

    @BuiltValueField(wireName: r'total')
    num get total;

    @BuiltValueField(wireName: r'page')
    num get page;

    @BuiltValueField(wireName: r'pageCount')
    num get pageCount;

    GetManyReviewResponseDto._();

    static void _initializeBuilder(GetManyReviewResponseDtoBuilder b) => b;

    factory GetManyReviewResponseDto([void updates(GetManyReviewResponseDtoBuilder b)]) = _$GetManyReviewResponseDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<GetManyReviewResponseDto> get serializer => _$GetManyReviewResponseDtoSerializer();
}

class _$GetManyReviewResponseDtoSerializer implements StructuredSerializer<GetManyReviewResponseDto> {
    @override
    final Iterable<Type> types = const [GetManyReviewResponseDto, _$GetManyReviewResponseDto];

    @override
    final String wireName = r'GetManyReviewResponseDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, GetManyReviewResponseDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'data')
            ..add(serializers.serialize(object.data,
                specifiedType: const FullType(BuiltList, [FullType(Review)])));
        result
            ..add(r'count')
            ..add(serializers.serialize(object.count,
                specifiedType: const FullType(num)));
        result
            ..add(r'total')
            ..add(serializers.serialize(object.total,
                specifiedType: const FullType(num)));
        result
            ..add(r'page')
            ..add(serializers.serialize(object.page,
                specifiedType: const FullType(num)));
        result
            ..add(r'pageCount')
            ..add(serializers.serialize(object.pageCount,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    GetManyReviewResponseDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = GetManyReviewResponseDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'data':
                    result.data.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Review)])) as BuiltList<Review>);
                    break;
                case r'count':
                    result.count = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'total':
                    result.total = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'page':
                    result.page = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'pageCount':
                    result.pageCount = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

