//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/category.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'get_many_category_response_dto.g.dart';



abstract class GetManyCategoryResponseDto implements Built<GetManyCategoryResponseDto, GetManyCategoryResponseDtoBuilder> {
    @BuiltValueField(wireName: r'data')
    BuiltList<Category> get data;

    @BuiltValueField(wireName: r'count')
    num get count;

    @BuiltValueField(wireName: r'total')
    num get total;

    @BuiltValueField(wireName: r'page')
    num get page;

    @BuiltValueField(wireName: r'pageCount')
    num get pageCount;

    GetManyCategoryResponseDto._();

    static void _initializeBuilder(GetManyCategoryResponseDtoBuilder b) => b;

    factory GetManyCategoryResponseDto([void updates(GetManyCategoryResponseDtoBuilder b)]) = _$GetManyCategoryResponseDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<GetManyCategoryResponseDto> get serializer => _$GetManyCategoryResponseDtoSerializer();
}

class _$GetManyCategoryResponseDtoSerializer implements StructuredSerializer<GetManyCategoryResponseDto> {
    @override
    final Iterable<Type> types = const [GetManyCategoryResponseDto, _$GetManyCategoryResponseDto];

    @override
    final String wireName = r'GetManyCategoryResponseDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, GetManyCategoryResponseDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'data')
            ..add(serializers.serialize(object.data,
                specifiedType: const FullType(BuiltList, [FullType(Category)])));
        result
            ..add(r'count')
            ..add(serializers.serialize(object.count,
                specifiedType: const FullType(num)));
        result
            ..add(r'total')
            ..add(serializers.serialize(object.total,
                specifiedType: const FullType(num)));
        result
            ..add(r'page')
            ..add(serializers.serialize(object.page,
                specifiedType: const FullType(num)));
        result
            ..add(r'pageCount')
            ..add(serializers.serialize(object.pageCount,
                specifiedType: const FullType(num)));
        return result;
    }

    @override
    GetManyCategoryResponseDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = GetManyCategoryResponseDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'data':
                    result.data.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Category)])) as BuiltList<Category>);
                    break;
                case r'count':
                    result.count = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'total':
                    result.total = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'page':
                    result.page = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'pageCount':
                    result.pageCount = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

