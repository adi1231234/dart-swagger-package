//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/range.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'availability_time.g.dart';



abstract class AvailabilityTime implements Built<AvailabilityTime, AvailabilityTimeBuilder> {
    @BuiltValueField(wireName: r'dayOfWeek')
    AvailabilityTimeDayOfWeekEnum get dayOfWeek;
    // enum dayOfWeekEnum {  sunday,  monday,  tuesday,  wednesday,  thursday,  friday,  saturday,  };

    @BuiltValueField(wireName: r'ranges')
    BuiltList<Range> get ranges;

    AvailabilityTime._();

    static void _initializeBuilder(AvailabilityTimeBuilder b) => b
        ..dayOfWeek = const AvailabilityTimeDayOfWeekEnum._('sunday');

    factory AvailabilityTime([void updates(AvailabilityTimeBuilder b)]) = _$AvailabilityTime;

    @BuiltValueSerializer(custom: true)
    static Serializer<AvailabilityTime> get serializer => _$AvailabilityTimeSerializer();
}

class _$AvailabilityTimeSerializer implements StructuredSerializer<AvailabilityTime> {
    @override
    final Iterable<Type> types = const [AvailabilityTime, _$AvailabilityTime];

    @override
    final String wireName = r'AvailabilityTime';

    @override
    Iterable<Object?> serialize(Serializers serializers, AvailabilityTime object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'dayOfWeek')
            ..add(serializers.serialize(object.dayOfWeek,
                specifiedType: const FullType(AvailabilityTimeDayOfWeekEnum)));
        result
            ..add(r'ranges')
            ..add(serializers.serialize(object.ranges,
                specifiedType: const FullType(BuiltList, [FullType(Range)])));
        return result;
    }

    @override
    AvailabilityTime deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = AvailabilityTimeBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'dayOfWeek':
                    result.dayOfWeek = serializers.deserialize(value,
                        specifiedType: const FullType(AvailabilityTimeDayOfWeekEnum)) as AvailabilityTimeDayOfWeekEnum;
                    break;
                case r'ranges':
                    result.ranges.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Range)])) as BuiltList<Range>);
                    break;
            }
        }
        return result.build();
    }
}

class AvailabilityTimeDayOfWeekEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'sunday')
  static const AvailabilityTimeDayOfWeekEnum sunday = _$availabilityTimeDayOfWeekEnum_sunday;
  @BuiltValueEnumConst(wireName: r'monday')
  static const AvailabilityTimeDayOfWeekEnum monday = _$availabilityTimeDayOfWeekEnum_monday;
  @BuiltValueEnumConst(wireName: r'tuesday')
  static const AvailabilityTimeDayOfWeekEnum tuesday = _$availabilityTimeDayOfWeekEnum_tuesday;
  @BuiltValueEnumConst(wireName: r'wednesday')
  static const AvailabilityTimeDayOfWeekEnum wednesday = _$availabilityTimeDayOfWeekEnum_wednesday;
  @BuiltValueEnumConst(wireName: r'thursday')
  static const AvailabilityTimeDayOfWeekEnum thursday = _$availabilityTimeDayOfWeekEnum_thursday;
  @BuiltValueEnumConst(wireName: r'friday')
  static const AvailabilityTimeDayOfWeekEnum friday = _$availabilityTimeDayOfWeekEnum_friday;
  @BuiltValueEnumConst(wireName: r'saturday')
  static const AvailabilityTimeDayOfWeekEnum saturday = _$availabilityTimeDayOfWeekEnum_saturday;

  static Serializer<AvailabilityTimeDayOfWeekEnum> get serializer => _$availabilityTimeDayOfWeekEnumSerializer;

  const AvailabilityTimeDayOfWeekEnum._(String name): super(name);

  static BuiltSet<AvailabilityTimeDayOfWeekEnum> get values => _$availabilityTimeDayOfWeekEnumValues;
  static AvailabilityTimeDayOfWeekEnum valueOf(String name) => _$availabilityTimeDayOfWeekEnumValueOf(name);
}

