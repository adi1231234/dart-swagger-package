//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:products_communicator/src/model/review.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'reviews_for_target.g.dart';



abstract class ReviewsForTarget implements Built<ReviewsForTarget, ReviewsForTargetBuilder> {
    @BuiltValueField(wireName: r'targetId')
    num get targetId;

    @BuiltValueField(wireName: r'targetType')
    ReviewsForTargetTargetTypeEnum get targetType;
    // enum targetTypeEnum {  product,  user,  };

    @BuiltValueField(wireName: r'mean')
    num get mean;

    @BuiltValueField(wireName: r'reviews')
    BuiltList<Review> get reviews;

    ReviewsForTarget._();

    static void _initializeBuilder(ReviewsForTargetBuilder b) => b;

    factory ReviewsForTarget([void updates(ReviewsForTargetBuilder b)]) = _$ReviewsForTarget;

    @BuiltValueSerializer(custom: true)
    static Serializer<ReviewsForTarget> get serializer => _$ReviewsForTargetSerializer();
}

class _$ReviewsForTargetSerializer implements StructuredSerializer<ReviewsForTarget> {
    @override
    final Iterable<Type> types = const [ReviewsForTarget, _$ReviewsForTarget];

    @override
    final String wireName = r'ReviewsForTarget';

    @override
    Iterable<Object?> serialize(Serializers serializers, ReviewsForTarget object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'targetId')
            ..add(serializers.serialize(object.targetId,
                specifiedType: const FullType(num)));
        result
            ..add(r'targetType')
            ..add(serializers.serialize(object.targetType,
                specifiedType: const FullType(ReviewsForTargetTargetTypeEnum)));
        result
            ..add(r'mean')
            ..add(serializers.serialize(object.mean,
                specifiedType: const FullType(num)));
        result
            ..add(r'reviews')
            ..add(serializers.serialize(object.reviews,
                specifiedType: const FullType(BuiltList, [FullType(Review)])));
        return result;
    }

    @override
    ReviewsForTarget deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ReviewsForTargetBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'targetId':
                    result.targetId = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'targetType':
                    result.targetType = serializers.deserialize(value,
                        specifiedType: const FullType(ReviewsForTargetTargetTypeEnum)) as ReviewsForTargetTargetTypeEnum;
                    break;
                case r'mean':
                    result.mean = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'reviews':
                    result.reviews.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Review)])) as BuiltList<Review>);
                    break;
            }
        }
        return result.build();
    }
}

class ReviewsForTargetTargetTypeEnum extends EnumClass {

  @BuiltValueEnumConst(wireName: r'product')
  static const ReviewsForTargetTargetTypeEnum product = _$reviewsForTargetTargetTypeEnum_product;
  @BuiltValueEnumConst(wireName: r'user')
  static const ReviewsForTargetTargetTypeEnum user = _$reviewsForTargetTargetTypeEnum_user;

  static Serializer<ReviewsForTargetTargetTypeEnum> get serializer => _$reviewsForTargetTargetTypeEnumSerializer;

  const ReviewsForTargetTargetTypeEnum._(String name): super(name);

  static BuiltSet<ReviewsForTargetTargetTypeEnum> get values => _$reviewsForTargetTargetTypeEnumValues;
  static ReviewsForTargetTargetTypeEnum valueOf(String name) => _$reviewsForTargetTargetTypeEnumValueOf(name);
}

