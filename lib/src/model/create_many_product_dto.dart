//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:products_communicator/src/model/product.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_many_product_dto.g.dart';



abstract class CreateManyProductDto implements Built<CreateManyProductDto, CreateManyProductDtoBuilder> {
    @BuiltValueField(wireName: r'bulk')
    BuiltList<Product> get bulk;

    CreateManyProductDto._();

    static void _initializeBuilder(CreateManyProductDtoBuilder b) => b;

    factory CreateManyProductDto([void updates(CreateManyProductDtoBuilder b)]) = _$CreateManyProductDto;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateManyProductDto> get serializer => _$CreateManyProductDtoSerializer();
}

class _$CreateManyProductDtoSerializer implements StructuredSerializer<CreateManyProductDto> {
    @override
    final Iterable<Type> types = const [CreateManyProductDto, _$CreateManyProductDto];

    @override
    final String wireName = r'CreateManyProductDto';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateManyProductDto object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'bulk')
            ..add(serializers.serialize(object.bulk,
                specifiedType: const FullType(BuiltList, [FullType(Product)])));
        return result;
    }

    @override
    CreateManyProductDto deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateManyProductDtoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'bulk':
                    result.bulk.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Product)])) as BuiltList<Product>);
                    break;
            }
        }
        return result.build();
    }
}

