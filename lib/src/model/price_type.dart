//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'price_type.g.dart';

class PriceType extends EnumClass {

  @BuiltValueEnumConst(wireName: r'daily')
  static const PriceType daily = _$daily;
  @BuiltValueEnumConst(wireName: r'hourly')
  static const PriceType hourly = _$hourly;

  static Serializer<PriceType> get serializer => _$priceTypeSerializer;

  const PriceType._(String name): super(name);

  static BuiltSet<PriceType> get values => _$values;
  static PriceType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class PriceTypeMixin = Object with _$PriceTypeMixin;

