//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'range.g.dart';



abstract class Range implements Built<Range, RangeBuilder> {
    @BuiltValueField(wireName: r'from')
    DateTime get from;

    @BuiltValueField(wireName: r'to')
    DateTime get to;

    Range._();

    static void _initializeBuilder(RangeBuilder b) => b;

    factory Range([void updates(RangeBuilder b)]) = _$Range;

    @BuiltValueSerializer(custom: true)
    static Serializer<Range> get serializer => _$RangeSerializer();
}

class _$RangeSerializer implements StructuredSerializer<Range> {
    @override
    final Iterable<Type> types = const [Range, _$Range];

    @override
    final String wireName = r'Range';

    @override
    Iterable<Object?> serialize(Serializers serializers, Range object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'from')
            ..add(serializers.serialize(object.from,
                specifiedType: const FullType(DateTime)));
        result
            ..add(r'to')
            ..add(serializers.serialize(object.to,
                specifiedType: const FullType(DateTime)));
        return result;
    }

    @override
    Range deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = RangeBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'from':
                    result.from = serializers.deserialize(value,
                        specifiedType: const FullType(DateTime)) as DateTime;
                    break;
                case r'to':
                    result.to = serializers.deserialize(value,
                        specifiedType: const FullType(DateTime)) as DateTime;
                    break;
            }
        }
        return result.build();
    }
}

