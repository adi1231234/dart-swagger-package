//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';

import 'package:products_communicator/src/model/availability_time.dart';
import 'package:products_communicator/src/model/category.dart';
import 'package:products_communicator/src/model/create_many_category_dto.dart';
import 'package:products_communicator/src/model/create_many_feed_option_dto.dart';
import 'package:products_communicator/src/model/create_many_product_dto.dart';
import 'package:products_communicator/src/model/create_many_review_dto.dart';
import 'package:products_communicator/src/model/feed_option.dart';
import 'package:products_communicator/src/model/get_many_category_response_dto.dart';
import 'package:products_communicator/src/model/get_many_feed_option_response_dto.dart';
import 'package:products_communicator/src/model/get_many_product_response_dto.dart';
import 'package:products_communicator/src/model/get_many_review_response_dto.dart';
import 'package:products_communicator/src/model/price_type.dart';
import 'package:products_communicator/src/model/product.dart';
import 'package:products_communicator/src/model/product_type.dart';
import 'package:products_communicator/src/model/query_dto.dart';
import 'package:products_communicator/src/model/range.dart';
import 'package:products_communicator/src/model/review.dart';
import 'package:products_communicator/src/model/review_target_type.dart';
import 'package:products_communicator/src/model/reviews_for_target.dart';
import 'package:products_communicator/src/model/saved_product.dart';
import 'package:products_communicator/src/model/suspend_product_dto.dart';

part 'serializers.g.dart';

@SerializersFor([
  AvailabilityTime,
  Category,
  CreateManyCategoryDto,
  CreateManyFeedOptionDto,
  CreateManyProductDto,
  CreateManyReviewDto,
  FeedOption,
  GetManyCategoryResponseDto,
  GetManyFeedOptionResponseDto,
  GetManyProductResponseDto,
  GetManyReviewResponseDto,
  PriceType,
  Product,
  ProductType,
  QueryDTO,
  Range,
  Review,
  ReviewTargetType,
  ReviewsForTarget,
  SavedProduct,
  SuspendProductDto,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Category)]),
        () => ListBuilder<Category>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Product)]),
        () => ListBuilder<Product>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(FeedOption)]),
        () => ListBuilder<FeedOption>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Category)]),
        () => ListBuilder<Category>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Product)]),
        () => ListBuilder<Product>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Review)]),
        () => ListBuilder<Review>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Product)]),
        () => ListBuilder<Product>(),
      )
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
