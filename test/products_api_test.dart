import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';


/// tests for ProductsApi
void main() {
  final instance = ProductsCommunicator().getProductsApi();

  group(ProductsApi, () {
    // Create multiple Products
    //
    //Future<BuiltList<Product>> createManyBaseProductsControllerProduct(CreateManyProductDto createManyProductDto) async
    test('test createManyBaseProductsControllerProduct', () async {
      // TODO
    });

    // Create a single Product
    //
    //Future<Product> createOneBaseProductsControllerProduct(Product product) async
    test('test createOneBaseProductsControllerProduct', () async {
      // TODO
    });

    // Delete a single Product
    //
    //Future deleteOneBaseProductsControllerProduct(num id) async
    test('test deleteOneBaseProductsControllerProduct', () async {
      // TODO
    });

    // Retrieve multiple Products
    //
    //Future<GetManyProductResponseDto> getManyBaseProductsControllerProduct({ BuiltList<String> fields, String s, BuiltList<String> filter, BuiltList<String> or, BuiltList<String> sort, BuiltList<String> join, int limit, int offset, int page, int cache }) async
    test('test getManyBaseProductsControllerProduct', () async {
      // TODO
    });

    // Retrieve a single Product
    //
    //Future<Product> getOneBaseProductsControllerProduct(num id, { BuiltList<String> fields, BuiltList<String> join, int cache }) async
    test('test getOneBaseProductsControllerProduct', () async {
      // TODO
    });

    // Get products by its category
    //
    //Future<BuiltList<Product>> productsControllerGetProductsByCategory(num id, bool expand) async
    test('test productsControllerGetProductsByCategory', () async {
      // TODO
    });

    // Get all products by product type
    //
    //Future<BuiltList<Product>> productsControllerGetProductsByType(ProductType type) async
    test('test productsControllerGetProductsByType', () async {
      // TODO
    });

    // Get all products by userId
    //
    //Future<BuiltList<Product>> productsControllerGetUserProducts(num id) async
    test('test productsControllerGetUserProducts', () async {
      // TODO
    });

    // Query products by object
    //
    //Future<BuiltList<Product>> productsControllerQueryProducts(QueryDTO queryDTO) async
    test('test productsControllerQueryProducts', () async {
      // TODO
    });

    // Suspends an product by its id
    //
    //Future<Product> productsControllerSuspendProduct(SuspendProductDto suspendProductDto) async
    test('test productsControllerSuspendProduct', () async {
      // TODO
    });

    // Replace a single Product
    //
    //Future<Product> replaceOneBaseProductsControllerProduct(num id, Product product) async
    test('test replaceOneBaseProductsControllerProduct', () async {
      // TODO
    });

    // Update a single Product
    //
    //Future<Product> updateOneBaseProductsControllerProduct(num id, Product product) async
    test('test updateOneBaseProductsControllerProduct', () async {
      // TODO
    });

  });
}
