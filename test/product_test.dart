import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';

// tests for Product
void main() {
  final instance = ProductBuilder();
  // TODO add properties to the builder and call build()

  group(Product, () {
    // num id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // BuiltList<String> imagePaths
    test('to test the property `imagePaths`', () async {
      // TODO
    });

    // num price
    test('to test the property `price`', () async {
      // TODO
    });

    // PriceType priceType
    test('to test the property `priceType`', () async {
      // TODO
    });

    // String ownerId
    test('to test the property `ownerId`', () async {
      // TODO
    });

    // ProductType productType
    test('to test the property `productType`', () async {
      // TODO
    });

    // num categoryId
    test('to test the property `categoryId`', () async {
      // TODO
    });

    // BuiltList<AvailabilityTime> availabilityTimes
    test('to test the property `availabilityTimes`', () async {
      // TODO
    });

    // bool suspended (default value: false)
    test('to test the property `suspended`', () async {
      // TODO
    });

  });
}
