import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';


/// tests for SavedProductsApi
void main() {
  final instance = ProductsCommunicator().getSavedProductsApi();

  group(SavedProductsApi, () {
    // Get saved products by user id
    //
    //Future<BuiltList<Product>> savedProductsControllerGetSavedProductsByUser(num userId) async
    test('test savedProductsControllerGetSavedProductsByUser', () async {
      // TODO
    });

    // Link a user to a new saved product
    //
    //Future<BuiltList<Product>> savedProductsControllerLinkUserToSavedProduct(SavedProduct savedProduct) async
    test('test savedProductsControllerLinkUserToSavedProduct', () async {
      // TODO
    });

  });
}
