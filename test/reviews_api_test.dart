import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';


/// tests for ReviewsApi
void main() {
  final instance = ProductsCommunicator().getReviewsApi();

  group(ReviewsApi, () {
    // Create multiple Reviews
    //
    //Future<BuiltList<Review>> createManyBaseReviewsControllerReview(CreateManyReviewDto createManyReviewDto) async
    test('test createManyBaseReviewsControllerReview', () async {
      // TODO
    });

    // Create a single Review
    //
    //Future<Review> createOneBaseReviewsControllerReview(Review review) async
    test('test createOneBaseReviewsControllerReview', () async {
      // TODO
    });

    // Delete a single Review
    //
    //Future deleteOneBaseReviewsControllerReview(num id) async
    test('test deleteOneBaseReviewsControllerReview', () async {
      // TODO
    });

    // Retrieve multiple Reviews
    //
    //Future<GetManyReviewResponseDto> getManyBaseReviewsControllerReview({ BuiltList<String> fields, String s, BuiltList<String> filter, BuiltList<String> or, BuiltList<String> sort, BuiltList<String> join, int limit, int offset, int page, int cache }) async
    test('test getManyBaseReviewsControllerReview', () async {
      // TODO
    });

    // Retrieve a single Review
    //
    //Future<Review> getOneBaseReviewsControllerReview(num id, { BuiltList<String> fields, BuiltList<String> join, int cache }) async
    test('test getOneBaseReviewsControllerReview', () async {
      // TODO
    });

    // Replace a single Review
    //
    //Future<Review> replaceOneBaseReviewsControllerReview(num id, Review review) async
    test('test replaceOneBaseReviewsControllerReview', () async {
      // TODO
    });

    // Get reviews by target id for targetType
    //
    //Future<ReviewsForTarget> reviewsControllerGetProductReviews(num targetId, ReviewTargetType type) async
    test('test reviewsControllerGetProductReviews', () async {
      // TODO
    });

    // Update a single Review
    //
    //Future<Review> updateOneBaseReviewsControllerReview(num id, Review review) async
    test('test updateOneBaseReviewsControllerReview', () async {
      // TODO
    });

  });
}
