import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';


/// tests for RecommendationsApi
void main() {
  final instance = ProductsCommunicator().getRecommendationsApi();

  group(RecommendationsApi, () {
    // Create multiple FeedOptions
    //
    //Future<BuiltList<FeedOption>> createManyBaseRecommendationsControllerFeedOption(CreateManyFeedOptionDto createManyFeedOptionDto) async
    test('test createManyBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

    // Create a single FeedOption
    //
    //Future<FeedOption> createOneBaseRecommendationsControllerFeedOption(FeedOption feedOption) async
    test('test createOneBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

    // Delete a single FeedOption
    //
    //Future deleteOneBaseRecommendationsControllerFeedOption(num id) async
    test('test deleteOneBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

    // Retrieve multiple FeedOptions
    //
    //Future<GetManyFeedOptionResponseDto> getManyBaseRecommendationsControllerFeedOption({ BuiltList<String> fields, String s, BuiltList<String> filter, BuiltList<String> or, BuiltList<String> sort, BuiltList<String> join, int limit, int offset, int page, int cache }) async
    test('test getManyBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

    // Retrieve a single FeedOption
    //
    //Future<FeedOption> getOneBaseRecommendationsControllerFeedOption(num id, { BuiltList<String> fields, BuiltList<String> join, int cache }) async
    test('test getOneBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

    // Get recommended categories by userId
    //
    //Future<BuiltList<Category>> recommendationsControllerGetRecommendedCategories(num userId) async
    test('test recommendationsControllerGetRecommendedCategories', () async {
      // TODO
    });

    // Get recommended feed by userId
    //
    //Future<BuiltList<FeedOption>> recommendationsControllerGetRecommendedFeedByUser(num userId) async
    test('test recommendationsControllerGetRecommendedFeedByUser', () async {
      // TODO
    });

    // Get recommended products by userId
    //
    //Future<BuiltList<Product>> recommendationsControllerGetRecommendedProducts(num userId) async
    test('test recommendationsControllerGetRecommendedProducts', () async {
      // TODO
    });

    // init feed to user
    //
    //Future<BuiltList<FeedOption>> recommendationsControllerInitFeed(num userId) async
    test('test recommendationsControllerInitFeed', () async {
      // TODO
    });

    // Replace a single FeedOption
    //
    //Future<FeedOption> replaceOneBaseRecommendationsControllerFeedOption(num id, FeedOption feedOption) async
    test('test replaceOneBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

    // Update a single FeedOption
    //
    //Future<FeedOption> updateOneBaseRecommendationsControllerFeedOption(num id, FeedOption feedOption) async
    test('test updateOneBaseRecommendationsControllerFeedOption', () async {
      // TODO
    });

  });
}
