import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';


/// tests for CategoriesApi
void main() {
  final instance = ProductsCommunicator().getCategoriesApi();

  group(CategoriesApi, () {
    // Get all child categories from category ID
    //
    //Future<BuiltList<Category>> categoriesControllerGetChildrenCategory(num id) async
    test('test categoriesControllerGetChildrenCategory', () async {
      // TODO
    });

    // Create multiple Categories
    //
    //Future<BuiltList<Category>> createManyBaseCategoriesControllerCategory(CreateManyCategoryDto createManyCategoryDto) async
    test('test createManyBaseCategoriesControllerCategory', () async {
      // TODO
    });

    // Create a single Category
    //
    //Future<Category> createOneBaseCategoriesControllerCategory(Category category) async
    test('test createOneBaseCategoriesControllerCategory', () async {
      // TODO
    });

    // Delete a single Category
    //
    //Future deleteOneBaseCategoriesControllerCategory(num id) async
    test('test deleteOneBaseCategoriesControllerCategory', () async {
      // TODO
    });

    // Retrieve multiple Categories
    //
    //Future<GetManyCategoryResponseDto> getManyBaseCategoriesControllerCategory({ BuiltList<String> fields, String s, BuiltList<String> filter, BuiltList<String> or, BuiltList<String> sort, BuiltList<String> join, int limit, int offset, int page, int cache }) async
    test('test getManyBaseCategoriesControllerCategory', () async {
      // TODO
    });

    // Retrieve a single Category
    //
    //Future<Category> getOneBaseCategoriesControllerCategory(num id, { BuiltList<String> fields, BuiltList<String> join, int cache }) async
    test('test getOneBaseCategoriesControllerCategory', () async {
      // TODO
    });

    // Replace a single Category
    //
    //Future<Category> replaceOneBaseCategoriesControllerCategory(num id, Category category) async
    test('test replaceOneBaseCategoriesControllerCategory', () async {
      // TODO
    });

    // Update a single Category
    //
    //Future<Category> updateOneBaseCategoriesControllerCategory(num id, Category category) async
    test('test updateOneBaseCategoriesControllerCategory', () async {
      // TODO
    });

  });
}
