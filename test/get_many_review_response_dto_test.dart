import 'package:test/test.dart';
import 'package:products_communicator/products_communicator.dart';

// tests for GetManyReviewResponseDto
void main() {
  final instance = GetManyReviewResponseDtoBuilder();
  // TODO add properties to the builder and call build()

  group(GetManyReviewResponseDto, () {
    // BuiltList<Review> data
    test('to test the property `data`', () async {
      // TODO
    });

    // num count
    test('to test the property `count`', () async {
      // TODO
    });

    // num total
    test('to test the property `total`', () async {
      // TODO
    });

    // num page
    test('to test the property `page`', () async {
      // TODO
    });

    // num pageCount
    test('to test the property `pageCount`', () async {
      // TODO
    });

  });
}
