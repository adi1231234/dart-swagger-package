# products_communicator.api.CategoriesApi

## Load the API package
```dart
import 'package:products_communicator/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesControllerGetChildrenCategory**](CategoriesApi.md#categoriescontrollergetchildrencategory) | **get** /categories/subCategories/{id} | Get all child categories from category ID
[**createManyBaseCategoriesControllerCategory**](CategoriesApi.md#createmanybasecategoriescontrollercategory) | **post** /categories/bulk | Create multiple Categories
[**createOneBaseCategoriesControllerCategory**](CategoriesApi.md#createonebasecategoriescontrollercategory) | **post** /categories | Create a single Category
[**deleteOneBaseCategoriesControllerCategory**](CategoriesApi.md#deleteonebasecategoriescontrollercategory) | **delete** /categories/{id} | Delete a single Category
[**getManyBaseCategoriesControllerCategory**](CategoriesApi.md#getmanybasecategoriescontrollercategory) | **get** /categories | Retrieve multiple Categories
[**getOneBaseCategoriesControllerCategory**](CategoriesApi.md#getonebasecategoriescontrollercategory) | **get** /categories/{id} | Retrieve a single Category
[**replaceOneBaseCategoriesControllerCategory**](CategoriesApi.md#replaceonebasecategoriescontrollercategory) | **put** /categories/{id} | Replace a single Category
[**updateOneBaseCategoriesControllerCategory**](CategoriesApi.md#updateonebasecategoriescontrollercategory) | **patch** /categories/{id} | Update a single Category


# **categoriesControllerGetChildrenCategory**
> BuiltList<Category> categoriesControllerGetChildrenCategory(id)

Get all child categories from category ID

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var id = 8.14; // num | 

try { 
    var result = api_instance.categoriesControllerGetChildrenCategory(id);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->categoriesControllerGetChildrenCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 

### Return type

[**BuiltList<Category>**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createManyBaseCategoriesControllerCategory**
> BuiltList<Category> createManyBaseCategoriesControllerCategory(createManyCategoryDto)

Create multiple Categories

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var createManyCategoryDto = new CreateManyCategoryDto(); // CreateManyCategoryDto | 

try { 
    var result = api_instance.createManyBaseCategoriesControllerCategory(createManyCategoryDto);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->createManyBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createManyCategoryDto** | [**CreateManyCategoryDto**](CreateManyCategoryDto.md)|  | 

### Return type

[**BuiltList<Category>**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createOneBaseCategoriesControllerCategory**
> Category createOneBaseCategoriesControllerCategory(category)

Create a single Category

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var category = new Category(); // Category | 

try { 
    var result = api_instance.createOneBaseCategoriesControllerCategory(category);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->createOneBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | [**Category**](Category.md)|  | 

### Return type

[**Category**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteOneBaseCategoriesControllerCategory**
> deleteOneBaseCategoriesControllerCategory(id)

Delete a single Category

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var id = 8.14; // num | 

try { 
    api_instance.deleteOneBaseCategoriesControllerCategory(id);
} catch (e) {
    print('Exception when calling CategoriesApi->deleteOneBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getManyBaseCategoriesControllerCategory**
> GetManyCategoryResponseDto getManyBaseCategoriesControllerCategory(fields, s, filter, or, sort, join, limit, offset, page, cache)

Retrieve multiple Categories

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var s = s_example; // String | Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a>
var filter = []; // BuiltList<String> | Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a>
var or = []; // BuiltList<String> | Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a>
var sort = []; // BuiltList<String> | Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var limit = 56; // int | Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a>
var offset = 56; // int | Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a>
var page = 56; // int | Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getManyBaseCategoriesControllerCategory(fields, s, filter, or, sort, join, limit, offset, page, cache);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->getManyBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **s** | **String**| Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a> | [optional] 
 **filter** | [**BuiltList<String>**](String.md)| Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a> | [optional] 
 **or** | [**BuiltList<String>**](String.md)| Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a> | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **limit** | **int**| Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a> | [optional] 
 **offset** | **int**| Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a> | [optional] 
 **page** | **int**| Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**GetManyCategoryResponseDto**](GetManyCategoryResponseDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneBaseCategoriesControllerCategory**
> Category getOneBaseCategoriesControllerCategory(id, fields, join, cache)

Retrieve a single Category

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var id = 8.14; // num | 
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getOneBaseCategoriesControllerCategory(id, fields, join, cache);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->getOneBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**Category**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replaceOneBaseCategoriesControllerCategory**
> Category replaceOneBaseCategoriesControllerCategory(id, category)

Replace a single Category

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var id = 8.14; // num | 
var category = new Category(); // Category | 

try { 
    var result = api_instance.replaceOneBaseCategoriesControllerCategory(id, category);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->replaceOneBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **category** | [**Category**](Category.md)|  | 

### Return type

[**Category**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateOneBaseCategoriesControllerCategory**
> Category updateOneBaseCategoriesControllerCategory(id, category)

Update a single Category

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new CategoriesApi();
var id = 8.14; // num | 
var category = new Category(); // Category | 

try { 
    var result = api_instance.updateOneBaseCategoriesControllerCategory(id, category);
    print(result);
} catch (e) {
    print('Exception when calling CategoriesApi->updateOneBaseCategoriesControllerCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **category** | [**Category**](Category.md)|  | 

### Return type

[**Category**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

