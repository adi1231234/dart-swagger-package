# products_communicator.api.SavedProductsApi

## Load the API package
```dart
import 'package:products_communicator/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**savedProductsControllerGetSavedProductsByUser**](SavedProductsApi.md#savedproductscontrollergetsavedproductsbyuser) | **get** /savedProducts/user/{userId} | Get saved products by user id
[**savedProductsControllerLinkUserToSavedProduct**](SavedProductsApi.md#savedproductscontrollerlinkusertosavedproduct) | **post** /savedProducts | Link a user to a new saved product


# **savedProductsControllerGetSavedProductsByUser**
> BuiltList<Product> savedProductsControllerGetSavedProductsByUser(userId)

Get saved products by user id

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new SavedProductsApi();
var userId = 8.14; // num | 

try { 
    var result = api_instance.savedProductsControllerGetSavedProductsByUser(userId);
    print(result);
} catch (e) {
    print('Exception when calling SavedProductsApi->savedProductsControllerGetSavedProductsByUser: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **num**|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **savedProductsControllerLinkUserToSavedProduct**
> BuiltList<Product> savedProductsControllerLinkUserToSavedProduct(savedProduct)

Link a user to a new saved product

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new SavedProductsApi();
var savedProduct = new SavedProduct(); // SavedProduct | 

try { 
    var result = api_instance.savedProductsControllerLinkUserToSavedProduct(savedProduct);
    print(result);
} catch (e) {
    print('Exception when calling SavedProductsApi->savedProductsControllerLinkUserToSavedProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **savedProduct** | [**SavedProduct**](SavedProduct.md)|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

