# products_communicator.api.SystemApi

## Load the API package
```dart
import 'package:products_communicator/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**systemControllerGetConfig**](SystemApi.md#systemcontrollergetconfig) | **get** /system/config | Get configuration


# **systemControllerGetConfig**
> systemControllerGetConfig()

Get configuration

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new SystemApi();

try { 
    api_instance.systemControllerGetConfig();
} catch (e) {
    print('Exception when calling SystemApi->systemControllerGetConfig: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

