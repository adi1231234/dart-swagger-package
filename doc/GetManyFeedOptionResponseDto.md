# products_communicator.model.GetManyFeedOptionResponseDto

## Load the model package
```dart
import 'package:products_communicator/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**BuiltList<FeedOption>**](FeedOption.md) |  | 
**count** | **num** |  | 
**total** | **num** |  | 
**page** | **num** |  | 
**pageCount** | **num** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


