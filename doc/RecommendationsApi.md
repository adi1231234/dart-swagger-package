# products_communicator.api.RecommendationsApi

## Load the API package
```dart
import 'package:products_communicator/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createManyBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#createmanybaserecommendationscontrollerfeedoption) | **post** /recommendations/bulk | Create multiple FeedOptions
[**createOneBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#createonebaserecommendationscontrollerfeedoption) | **post** /recommendations | Create a single FeedOption
[**deleteOneBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#deleteonebaserecommendationscontrollerfeedoption) | **delete** /recommendations/{id} | Delete a single FeedOption
[**getManyBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#getmanybaserecommendationscontrollerfeedoption) | **get** /recommendations | Retrieve multiple FeedOptions
[**getOneBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#getonebaserecommendationscontrollerfeedoption) | **get** /recommendations/{id} | Retrieve a single FeedOption
[**recommendationsControllerGetRecommendedCategories**](RecommendationsApi.md#recommendationscontrollergetrecommendedcategories) | **get** /recommendations/recommendedCategories/{userId} | Get recommended categories by userId
[**recommendationsControllerGetRecommendedFeedByUser**](RecommendationsApi.md#recommendationscontrollergetrecommendedfeedbyuser) | **get** /recommendations/recommendedFeedByUser/{userId} | Get recommended feed by userId
[**recommendationsControllerGetRecommendedProducts**](RecommendationsApi.md#recommendationscontrollergetrecommendedproducts) | **get** /recommendations/recommendedProducts/{userId} | Get recommended products by userId
[**recommendationsControllerInitFeed**](RecommendationsApi.md#recommendationscontrollerinitfeed) | **get** /recommendations/initFeed/{userId} | init feed to user
[**replaceOneBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#replaceonebaserecommendationscontrollerfeedoption) | **put** /recommendations/{id} | Replace a single FeedOption
[**updateOneBaseRecommendationsControllerFeedOption**](RecommendationsApi.md#updateonebaserecommendationscontrollerfeedoption) | **patch** /recommendations/{id} | Update a single FeedOption


# **createManyBaseRecommendationsControllerFeedOption**
> BuiltList<FeedOption> createManyBaseRecommendationsControllerFeedOption(createManyFeedOptionDto)

Create multiple FeedOptions

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var createManyFeedOptionDto = new CreateManyFeedOptionDto(); // CreateManyFeedOptionDto | 

try { 
    var result = api_instance.createManyBaseRecommendationsControllerFeedOption(createManyFeedOptionDto);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->createManyBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createManyFeedOptionDto** | [**CreateManyFeedOptionDto**](CreateManyFeedOptionDto.md)|  | 

### Return type

[**BuiltList<FeedOption>**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createOneBaseRecommendationsControllerFeedOption**
> FeedOption createOneBaseRecommendationsControllerFeedOption(feedOption)

Create a single FeedOption

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var feedOption = new FeedOption(); // FeedOption | 

try { 
    var result = api_instance.createOneBaseRecommendationsControllerFeedOption(feedOption);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->createOneBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **feedOption** | [**FeedOption**](FeedOption.md)|  | 

### Return type

[**FeedOption**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteOneBaseRecommendationsControllerFeedOption**
> deleteOneBaseRecommendationsControllerFeedOption(id)

Delete a single FeedOption

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var id = 8.14; // num | 

try { 
    api_instance.deleteOneBaseRecommendationsControllerFeedOption(id);
} catch (e) {
    print('Exception when calling RecommendationsApi->deleteOneBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getManyBaseRecommendationsControllerFeedOption**
> GetManyFeedOptionResponseDto getManyBaseRecommendationsControllerFeedOption(fields, s, filter, or, sort, join, limit, offset, page, cache)

Retrieve multiple FeedOptions

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var s = s_example; // String | Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a>
var filter = []; // BuiltList<String> | Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a>
var or = []; // BuiltList<String> | Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a>
var sort = []; // BuiltList<String> | Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var limit = 56; // int | Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a>
var offset = 56; // int | Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a>
var page = 56; // int | Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getManyBaseRecommendationsControllerFeedOption(fields, s, filter, or, sort, join, limit, offset, page, cache);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->getManyBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **s** | **String**| Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a> | [optional] 
 **filter** | [**BuiltList<String>**](String.md)| Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a> | [optional] 
 **or** | [**BuiltList<String>**](String.md)| Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a> | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **limit** | **int**| Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a> | [optional] 
 **offset** | **int**| Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a> | [optional] 
 **page** | **int**| Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**GetManyFeedOptionResponseDto**](GetManyFeedOptionResponseDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneBaseRecommendationsControllerFeedOption**
> FeedOption getOneBaseRecommendationsControllerFeedOption(id, fields, join, cache)

Retrieve a single FeedOption

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var id = 8.14; // num | 
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getOneBaseRecommendationsControllerFeedOption(id, fields, join, cache);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->getOneBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**FeedOption**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recommendationsControllerGetRecommendedCategories**
> BuiltList<Category> recommendationsControllerGetRecommendedCategories(userId)

Get recommended categories by userId

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var userId = 8.14; // num | 

try { 
    var result = api_instance.recommendationsControllerGetRecommendedCategories(userId);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->recommendationsControllerGetRecommendedCategories: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **num**|  | 

### Return type

[**BuiltList<Category>**](Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recommendationsControllerGetRecommendedFeedByUser**
> BuiltList<FeedOption> recommendationsControllerGetRecommendedFeedByUser(userId)

Get recommended feed by userId

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var userId = 8.14; // num | 

try { 
    var result = api_instance.recommendationsControllerGetRecommendedFeedByUser(userId);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->recommendationsControllerGetRecommendedFeedByUser: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **num**|  | 

### Return type

[**BuiltList<FeedOption>**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recommendationsControllerGetRecommendedProducts**
> BuiltList<Product> recommendationsControllerGetRecommendedProducts(userId)

Get recommended products by userId

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var userId = 8.14; // num | 

try { 
    var result = api_instance.recommendationsControllerGetRecommendedProducts(userId);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->recommendationsControllerGetRecommendedProducts: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **num**|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recommendationsControllerInitFeed**
> BuiltList<FeedOption> recommendationsControllerInitFeed(userId)

init feed to user

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var userId = 8.14; // num | 

try { 
    var result = api_instance.recommendationsControllerInitFeed(userId);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->recommendationsControllerInitFeed: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **num**|  | 

### Return type

[**BuiltList<FeedOption>**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replaceOneBaseRecommendationsControllerFeedOption**
> FeedOption replaceOneBaseRecommendationsControllerFeedOption(id, feedOption)

Replace a single FeedOption

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var id = 8.14; // num | 
var feedOption = new FeedOption(); // FeedOption | 

try { 
    var result = api_instance.replaceOneBaseRecommendationsControllerFeedOption(id, feedOption);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->replaceOneBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **feedOption** | [**FeedOption**](FeedOption.md)|  | 

### Return type

[**FeedOption**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateOneBaseRecommendationsControllerFeedOption**
> FeedOption updateOneBaseRecommendationsControllerFeedOption(id, feedOption)

Update a single FeedOption

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new RecommendationsApi();
var id = 8.14; // num | 
var feedOption = new FeedOption(); // FeedOption | 

try { 
    var result = api_instance.updateOneBaseRecommendationsControllerFeedOption(id, feedOption);
    print(result);
} catch (e) {
    print('Exception when calling RecommendationsApi->updateOneBaseRecommendationsControllerFeedOption: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **feedOption** | [**FeedOption**](FeedOption.md)|  | 

### Return type

[**FeedOption**](FeedOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

