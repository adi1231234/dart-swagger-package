# products_communicator.model.Product

## Load the model package
```dart
import 'package:products_communicator/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**imagePaths** | **BuiltList<String>** |  | 
**price** | **num** |  | 
**priceType** | [**PriceType**](PriceType.md) |  | 
**ownerId** | **String** |  | 
**productType** | [**ProductType**](ProductType.md) |  | 
**categoryId** | **num** |  | 
**availabilityTimes** | [**BuiltList<AvailabilityTime>**](AvailabilityTime.md) |  | 
**suspended** | **bool** |  | [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


