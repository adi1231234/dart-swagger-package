# products_communicator.api.ReviewsApi

## Load the API package
```dart
import 'package:products_communicator/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createManyBaseReviewsControllerReview**](ReviewsApi.md#createmanybasereviewscontrollerreview) | **post** /reviews/bulk | Create multiple Reviews
[**createOneBaseReviewsControllerReview**](ReviewsApi.md#createonebasereviewscontrollerreview) | **post** /reviews | Create a single Review
[**deleteOneBaseReviewsControllerReview**](ReviewsApi.md#deleteonebasereviewscontrollerreview) | **delete** /reviews/{id} | Delete a single Review
[**getManyBaseReviewsControllerReview**](ReviewsApi.md#getmanybasereviewscontrollerreview) | **get** /reviews | Retrieve multiple Reviews
[**getOneBaseReviewsControllerReview**](ReviewsApi.md#getonebasereviewscontrollerreview) | **get** /reviews/{id} | Retrieve a single Review
[**replaceOneBaseReviewsControllerReview**](ReviewsApi.md#replaceonebasereviewscontrollerreview) | **put** /reviews/{id} | Replace a single Review
[**reviewsControllerGetProductReviews**](ReviewsApi.md#reviewscontrollergetproductreviews) | **get** /reviews/perTarget/{targetId} | Get reviews by target id for targetType
[**updateOneBaseReviewsControllerReview**](ReviewsApi.md#updateonebasereviewscontrollerreview) | **patch** /reviews/{id} | Update a single Review


# **createManyBaseReviewsControllerReview**
> BuiltList<Review> createManyBaseReviewsControllerReview(createManyReviewDto)

Create multiple Reviews

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var createManyReviewDto = new CreateManyReviewDto(); // CreateManyReviewDto | 

try { 
    var result = api_instance.createManyBaseReviewsControllerReview(createManyReviewDto);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->createManyBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createManyReviewDto** | [**CreateManyReviewDto**](CreateManyReviewDto.md)|  | 

### Return type

[**BuiltList<Review>**](Review.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createOneBaseReviewsControllerReview**
> Review createOneBaseReviewsControllerReview(review)

Create a single Review

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var review = new Review(); // Review | 

try { 
    var result = api_instance.createOneBaseReviewsControllerReview(review);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->createOneBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **review** | [**Review**](Review.md)|  | 

### Return type

[**Review**](Review.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteOneBaseReviewsControllerReview**
> deleteOneBaseReviewsControllerReview(id)

Delete a single Review

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var id = 8.14; // num | 

try { 
    api_instance.deleteOneBaseReviewsControllerReview(id);
} catch (e) {
    print('Exception when calling ReviewsApi->deleteOneBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getManyBaseReviewsControllerReview**
> GetManyReviewResponseDto getManyBaseReviewsControllerReview(fields, s, filter, or, sort, join, limit, offset, page, cache)

Retrieve multiple Reviews

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var s = s_example; // String | Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a>
var filter = []; // BuiltList<String> | Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a>
var or = []; // BuiltList<String> | Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a>
var sort = []; // BuiltList<String> | Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var limit = 56; // int | Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a>
var offset = 56; // int | Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a>
var page = 56; // int | Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getManyBaseReviewsControllerReview(fields, s, filter, or, sort, join, limit, offset, page, cache);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->getManyBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **s** | **String**| Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a> | [optional] 
 **filter** | [**BuiltList<String>**](String.md)| Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a> | [optional] 
 **or** | [**BuiltList<String>**](String.md)| Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a> | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **limit** | **int**| Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a> | [optional] 
 **offset** | **int**| Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a> | [optional] 
 **page** | **int**| Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**GetManyReviewResponseDto**](GetManyReviewResponseDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneBaseReviewsControllerReview**
> Review getOneBaseReviewsControllerReview(id, fields, join, cache)

Retrieve a single Review

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var id = 8.14; // num | 
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getOneBaseReviewsControllerReview(id, fields, join, cache);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->getOneBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**Review**](Review.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replaceOneBaseReviewsControllerReview**
> Review replaceOneBaseReviewsControllerReview(id, review)

Replace a single Review

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var id = 8.14; // num | 
var review = new Review(); // Review | 

try { 
    var result = api_instance.replaceOneBaseReviewsControllerReview(id, review);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->replaceOneBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **review** | [**Review**](Review.md)|  | 

### Return type

[**Review**](Review.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reviewsControllerGetProductReviews**
> ReviewsForTarget reviewsControllerGetProductReviews(targetId, type)

Get reviews by target id for targetType

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var targetId = 8.14; // num | 
var type = ; // ReviewTargetType | 

try { 
    var result = api_instance.reviewsControllerGetProductReviews(targetId, type);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->reviewsControllerGetProductReviews: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **targetId** | **num**|  | 
 **type** | [**ReviewTargetType**](.md)|  | 

### Return type

[**ReviewsForTarget**](ReviewsForTarget.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateOneBaseReviewsControllerReview**
> Review updateOneBaseReviewsControllerReview(id, review)

Update a single Review

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ReviewsApi();
var id = 8.14; // num | 
var review = new Review(); // Review | 

try { 
    var result = api_instance.updateOneBaseReviewsControllerReview(id, review);
    print(result);
} catch (e) {
    print('Exception when calling ReviewsApi->updateOneBaseReviewsControllerReview: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **review** | [**Review**](Review.md)|  | 

### Return type

[**Review**](Review.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

