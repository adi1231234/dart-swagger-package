# products_communicator.model.FeedOption

## Load the model package
```dart
import 'package:products_communicator/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rank** | **num** |  | 
**type** | **String** |  | 
**userId** | **num** |  | 
**config** | **num** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


