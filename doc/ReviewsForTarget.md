# products_communicator.model.ReviewsForTarget

## Load the model package
```dart
import 'package:products_communicator/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targetId** | **num** |  | 
**targetType** | **String** |  | 
**mean** | **num** |  | 
**reviews** | [**BuiltList<Review>**](Review.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


