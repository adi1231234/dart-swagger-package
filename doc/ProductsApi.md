# products_communicator.api.ProductsApi

## Load the API package
```dart
import 'package:products_communicator/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createManyBaseProductsControllerProduct**](ProductsApi.md#createmanybaseproductscontrollerproduct) | **post** /products/bulk | Create multiple Products
[**createOneBaseProductsControllerProduct**](ProductsApi.md#createonebaseproductscontrollerproduct) | **post** /products | Create a single Product
[**deleteOneBaseProductsControllerProduct**](ProductsApi.md#deleteonebaseproductscontrollerproduct) | **delete** /products/{id} | Delete a single Product
[**getManyBaseProductsControllerProduct**](ProductsApi.md#getmanybaseproductscontrollerproduct) | **get** /products | Retrieve multiple Products
[**getOneBaseProductsControllerProduct**](ProductsApi.md#getonebaseproductscontrollerproduct) | **get** /products/{id} | Retrieve a single Product
[**productsControllerGetProductsByCategory**](ProductsApi.md#productscontrollergetproductsbycategory) | **get** /products/category/{id} | Get products by its category
[**productsControllerGetProductsByType**](ProductsApi.md#productscontrollergetproductsbytype) | **get** /products/productType | Get all products by product type
[**productsControllerGetUserProducts**](ProductsApi.md#productscontrollergetuserproducts) | **get** /products/user/{id} | Get all products by userId
[**productsControllerQueryProducts**](ProductsApi.md#productscontrollerqueryproducts) | **post** /products/query | Query products by object
[**productsControllerSuspendProduct**](ProductsApi.md#productscontrollersuspendproduct) | **post** /products/suspend | Suspends an product by its id
[**replaceOneBaseProductsControllerProduct**](ProductsApi.md#replaceonebaseproductscontrollerproduct) | **put** /products/{id} | Replace a single Product
[**updateOneBaseProductsControllerProduct**](ProductsApi.md#updateonebaseproductscontrollerproduct) | **patch** /products/{id} | Update a single Product


# **createManyBaseProductsControllerProduct**
> BuiltList<Product> createManyBaseProductsControllerProduct(createManyProductDto)

Create multiple Products

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var createManyProductDto = new CreateManyProductDto(); // CreateManyProductDto | 

try { 
    var result = api_instance.createManyBaseProductsControllerProduct(createManyProductDto);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->createManyBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createManyProductDto** | [**CreateManyProductDto**](CreateManyProductDto.md)|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createOneBaseProductsControllerProduct**
> Product createOneBaseProductsControllerProduct(product)

Create a single Product

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var product = new Product(); // Product | 

try { 
    var result = api_instance.createOneBaseProductsControllerProduct(product);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->createOneBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product** | [**Product**](Product.md)|  | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteOneBaseProductsControllerProduct**
> deleteOneBaseProductsControllerProduct(id)

Delete a single Product

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var id = 8.14; // num | 

try { 
    api_instance.deleteOneBaseProductsControllerProduct(id);
} catch (e) {
    print('Exception when calling ProductsApi->deleteOneBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getManyBaseProductsControllerProduct**
> GetManyProductResponseDto getManyBaseProductsControllerProduct(fields, s, filter, or, sort, join, limit, offset, page, cache)

Retrieve multiple Products

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var s = s_example; // String | Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a>
var filter = []; // BuiltList<String> | Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a>
var or = []; // BuiltList<String> | Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a>
var sort = []; // BuiltList<String> | Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var limit = 56; // int | Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a>
var offset = 56; // int | Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a>
var page = 56; // int | Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getManyBaseProductsControllerProduct(fields, s, filter, or, sort, join, limit, offset, page, cache);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->getManyBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **s** | **String**| Adds search condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#search\" target=\"_blank\">Docs</a> | [optional] 
 **filter** | [**BuiltList<String>**](String.md)| Adds filter condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#filter\" target=\"_blank\">Docs</a> | [optional] 
 **or** | [**BuiltList<String>**](String.md)| Adds OR condition. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#or\" target=\"_blank\">Docs</a> | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Adds sort by field. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#sort\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **limit** | **int**| Limit amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#limit\" target=\"_blank\">Docs</a> | [optional] 
 **offset** | **int**| Offset amount of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#offset\" target=\"_blank\">Docs</a> | [optional] 
 **page** | **int**| Page portion of resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#page\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**GetManyProductResponseDto**](GetManyProductResponseDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneBaseProductsControllerProduct**
> Product getOneBaseProductsControllerProduct(id, fields, join, cache)

Retrieve a single Product

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var id = 8.14; // num | 
var fields = []; // BuiltList<String> | Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a>
var join = []; // BuiltList<String> | Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a>
var cache = 56; // int | Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a>

try { 
    var result = api_instance.getOneBaseProductsControllerProduct(id, fields, join, cache);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->getOneBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **fields** | [**BuiltList<String>**](String.md)| Selects resource fields. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#select\" target=\"_blank\">Docs</a> | [optional] 
 **join** | [**BuiltList<String>**](String.md)| Adds relational resources. <a href=\"https://github.com/nestjsx/crud/wiki/Requests#join\" target=\"_blank\">Docs</a> | [optional] 
 **cache** | **int**| Reset cache (if was enabled). <a href=\"https://github.com/nestjsx/crud/wiki/Requests#cache\" target=\"_blank\">Docs</a> | [optional] 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **productsControllerGetProductsByCategory**
> BuiltList<Product> productsControllerGetProductsByCategory(id, expand)

Get products by its category

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var id = 8.14; // num | 
var expand = true; // bool | 

try { 
    var result = api_instance.productsControllerGetProductsByCategory(id, expand);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->productsControllerGetProductsByCategory: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **expand** | **bool**|  | [default to false]

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **productsControllerGetProductsByType**
> BuiltList<Product> productsControllerGetProductsByType(type)

Get all products by product type

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var type = ; // ProductType | 

try { 
    var result = api_instance.productsControllerGetProductsByType(type);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->productsControllerGetProductsByType: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | [**ProductType**](.md)|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **productsControllerGetUserProducts**
> BuiltList<Product> productsControllerGetUserProducts(id)

Get all products by userId

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var id = 8.14; // num | 

try { 
    var result = api_instance.productsControllerGetUserProducts(id);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->productsControllerGetUserProducts: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **productsControllerQueryProducts**
> BuiltList<Product> productsControllerQueryProducts(queryDTO)

Query products by object

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var queryDTO = new QueryDTO(); // QueryDTO | 

try { 
    var result = api_instance.productsControllerQueryProducts(queryDTO);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->productsControllerQueryProducts: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queryDTO** | [**QueryDTO**](QueryDTO.md)|  | 

### Return type

[**BuiltList<Product>**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **productsControllerSuspendProduct**
> Product productsControllerSuspendProduct(suspendProductDto)

Suspends an product by its id

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var suspendProductDto = new SuspendProductDto(); // SuspendProductDto | 

try { 
    var result = api_instance.productsControllerSuspendProduct(suspendProductDto);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->productsControllerSuspendProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suspendProductDto** | [**SuspendProductDto**](SuspendProductDto.md)|  | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **replaceOneBaseProductsControllerProduct**
> Product replaceOneBaseProductsControllerProduct(id, product)

Replace a single Product

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var id = 8.14; // num | 
var product = new Product(); // Product | 

try { 
    var result = api_instance.replaceOneBaseProductsControllerProduct(id, product);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->replaceOneBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **product** | [**Product**](Product.md)|  | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateOneBaseProductsControllerProduct**
> Product updateOneBaseProductsControllerProduct(id, product)

Update a single Product

### Example 
```dart
import 'package:products_communicator/api.dart';

var api_instance = new ProductsApi();
var id = 8.14; // num | 
var product = new Product(); // Product | 

try { 
    var result = api_instance.updateOneBaseProductsControllerProduct(id, product);
    print(result);
} catch (e) {
    print('Exception when calling ProductsApi->updateOneBaseProductsControllerProduct: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**|  | 
 **product** | [**Product**](Product.md)|  | 

### Return type

[**Product**](Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

